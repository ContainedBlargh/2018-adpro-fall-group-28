package adpro

import org.scalatest.FunSuite
import Stream._
import java.lang.StackOverflowError;

class Stream_test extends FunSuite {

  test("This one always works: (-1) * (-1) = 1") {
    assert((-1) * (-1) == 1);
  }

  //Sanity check of exercise 1
  test("The first element of Stream.from(3) is 3") {
    assert(from(3).headOption().contains(3));
  }

  test("The second element of Stream.from(3) is 4") {
    assert(from(3).tail.headOption().contains(4));
  }

  test("The first element of Stream.to(3) is 3") {
    assert(to(3).headOption().contains(3));
  }

  test("The second element of Stream.to(3) is 2") {
    assert(to(3).tail.headOption().contains(2));
  }

  test("The first element of naturals is 0") {
    assert(naturals.headOption().contains(0))
  }


  test("The Stream(1,2,3).toList is List(1,2,3) ") {
    val l2: Stream[Int] = cons(1, cons(2, cons(3, empty)))
    assert(l2.toList(0) == 1)
    assert(l2.toList(1) == 2)
    assert(l2.toList(2) == 3)
  }

  test("naturals.take(3)  is Stream(1,2,3) ") {
    val l2: Stream[Int] = naturals.take(3)
    assert(l2.toList(0) == 0)
    assert(l2.toList(1) == 1)
    assert(l2.toList(2) == 2)
  }

  test("naturals.drop(3) is Stream(3,4,5,...) ") {
    val l2: Stream[Int] = naturals.drop(3)
    assert(l2.headOption().contains(3))
    assert(l2.tail.headOption().contains(4))
    assert(l2.tail.tail.headOption().contains(5))

    //assert(l2.toList(2) == 5)
  }

  test("naturals.takeWhile(_ < 1000000000).drop(100).take(50).toList is List(100,101, ...)") {
    val list = naturals.takeWhile(_ < 1000000000).drop(100).take(50).toList
    assert(list.length.equals(50))
    for (i <- 0 to 49) assert(list(i).equals(i + 100))
  }

  test("naturals.forAll (_ < 0) returns false") {
    val allLessThan0 = naturals.forAll(_ < 0)
    assert(allLessThan0.equals(false))
  }

  test("naturals.forAll (_ >= 0) crashes with StackOverflowError") {
    try {
      naturals.forAll(_ >= 0)
    } catch {
      case ex: StackOverflowError => assert(true)
      case _: Throwable => assert(false)
    }
  }

  test("naturals.takeWhile2(_ < 1000000000).drop(100).take(50).toList is List(100,101, ...)") {
    val list = naturals.takeWhile2(_ < 1000000000).drop(100).take(50).toList
    assert(list.length.equals(50))
    for (i <- 0 to 49) assert(list(i).equals(i + 100))
  }

  test("The headOption2() of Stream.from(3) is 3") {
    assert(from(3).headOption2().contains(3));
  }

  test("The second element with headOption2() of Stream.from(3) is 4") {
    assert(from(3).tail.headOption2().contains(4));
  }

  test("The headOption2() of Stream.to(3) is 3") {
    assert(to(3).headOption2().contains(3));
  }

  test("The second element with headOption2() of Stream.to(3) is 2") {
    assert(to(3).tail.headOption2().contains(2));
  }

  test("The headOption2() of naturals is 0") {
    assert(naturals.headOption2().contains(0))
  }

  test("naturals.map (_*2).drop (30).take (50).toList") {
    val list = naturals.map(_ * 2).drop(30).take(50).toList
    for (i <- 0 to 49) assert(list(i).equals((i + 30) * 2))
  }

  test("Test case: naturals.drop(42).filter (_%2 ==0).take (30).toList") {
    val list = naturals.drop(42).filter(_ % 2 == 0).take(30).toList
    for (i <- 0 to 29) {
      val expected = 42 + i * 2
      val actual = list(i)
      assertResult(expected) {
        actual
      }
    }
  }

  test("Test case: naturals.append (naturals) (useless, but should not crash)") {
    try {
      naturals.append(naturals)
      assert(true)
    } catch {
      case _: Throwable => assert(false)
    }
  }

  test("Test case: naturals.take(10).append(naturals).take(20).toList") {
    val list = naturals.take(10).append(naturals).take(20).toList
    for (i <- 0 to 19) {
      val expected = i % 10
      val actual = list(i)
      assertResult(expected) {
        actual
      }
    }
  }

  test("Test case: naturals.flatMap (to _).take (100).toList") {
//    val list = naturals.flatMap(to _).take(100).toList
//    Causes stackoverflow error
    val list = naturals.take(100).flatMap(to).take(200).toList
  }

  test("Test case: naturals.flatMap (x =>from (x)).take (100).toList") {

  }
}