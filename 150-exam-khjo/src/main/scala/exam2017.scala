// Your name and ITU email: ____
package adpro.exam2017

import scala.language.higherKinds
import fpinscala.monoids.Monoid
import fpinscala.monads.Functor
import fpinscala.state.State
import fpinscala.laziness.{Stream,Empty,Cons}
import fpinscala.laziness.Stream._
import fpinscala.parallelism.Par._
import adpro.data._
import adpro.data.FingerTree._
import monocle.Lens

object Q1 { 

  def hasKey[K,V] (l: List[(K,V)]) (k: K) :Boolean = l.foldLeft (false)
  {case (acc: Boolean, (ek: K, ev: V)) => if (acc) acc else ek == k}

  def reduceByKey[K,V] (l :List[(K,V)]) (ope: (V,V) => V) :List[(K,V)] = l.foldLeft (List[(K, V)]()) {
    case (acc: List[(K,V)], (k: K, v: V)) =>
      if (hasKey(acc)(k)) acc.map (e => if (e._1 == k) (k, ope(v, e._2)) else e)
      else (k,v)::acc
  }


  def separate (l :List[(Int,List[String])]) :List[(Int,String)] =
    l flatMap { idws => idws._2 map { w => (idws._1,w) } }

  def separateViaFor (l :List[(Int,List[String])]) :List[(Int,String)] = for {
    idws <- l
    w <- idws._2
  } yield (idws._1, w)

} // Q1


object Q2 {

  trait TreeOfLists[+A]
  case object LeafOfLists  extends TreeOfLists[Nothing]
  case class BranchOfLists[+A] (
    data: List[A],
    left: TreeOfLists[A],
    right: TreeOfLists[A]
  ) extends TreeOfLists[A]

   trait TreeOfCollections[C[+_], +A]
   case class LeafOfCollections[C[+_]] () extends TreeOfCollections[C, Nothing]
   case class BranchOfCollections[C[+_], +A] (
     data: C[A],
     left: TreeOfCollections[C, A],
     right: TreeOfCollections[C, A]
   ) extends TreeOfCollections[C, A]

  def map[A,B] (t: TreeOfLists[A]) (f: A => B) :TreeOfLists[B] = t match {
    case LeafOfLists => LeafOfLists
    case BranchOfLists (data,left,right) =>
        BranchOfLists (data map f, map (left) (f), map (right) (f))
  }

   def map[C[_], A, B] (t: TreeOfCollections[C, A]) (f: A => B) (implicit functor: Functor[C]): TreeOfCollections[C, B] =
     t match {
       case LeafOfCollections() => LeafOfCollections[C] ()
       case BranchOfCollections(data, left, right) =>
         BranchOfCollections (functor.map[A,B] (data) (f), map(left) (f), map(right) (f))
     }

} // Q2

object Q3 {

  def p (n: Int): Int = { println (n.toString); n }

  def f (a: Int, b: Int): Int = if (a > 10) a else b

  // Answer the questions in comments here

  // A.
  // 42
  // 7
  // 42

  // B.
  // 42
  // 42
  // 42

  // C.
  // 42
  // 42

} // Q3


object Q4 {

  sealed trait Input
  case object Coin extends Input
  case object Brew extends Input

  case class MachineState (ready: Boolean, coffee: Int, coins: Int)

  def step (i: Input) (s: MachineState) :MachineState = i match {
    case _ if s.coffee == 0 => s
    case Coin => MachineState(false, s.coffee, s.coins + 1)
    case Brew if !s.ready => MachineState(true, s.coffee - 1, s.coins)
    case Brew if s.ready => s
  }

  def simulateMachine (initial: MachineState) (inputs: List[Input]) :(Int,Int) = {
    //convert inputs to state changes
    val actions: List[State[MachineState, Unit]] = inputs.map(i => State.modify(step (i)))

    //set up a sequence of state changes and run them, with the initial state. Fetch end state
    val finalState: MachineState = State.sequence(actions).run(initial)._2

    //return values needed
    (finalState.coffee, finalState.coins)
  }

} // Q4


object Q5 {

  def flatten[A] (s: =>Stream[List[A]]) :Stream[A] = s match {
    case Empty => empty
    case Cons(h, t) =>
      lazy val l = h()
      l match {
        case Nil => flatten(t())
        case head::tl => Stream.cons(head, flatten(Cons(() => tl, t)))
      }
  }

} // Q5


object Q6 {

  def parExists[A] (as: List[A]) (p: A => Boolean): Par[Boolean] = map(parMap(as)(p))(l => l.exists(e => e))

} // Q6


object Q7 {

  //  def reduceL[A,Z] (opl: (Z,A) => Z) (z: Z, t: FingerTree[A]) :Z = ??? // assume that this is implemented
  //  def reduceR[A,Z] (opr: (A,Z) => Z) (t: FingerTree[A], z: Z) :Z = ??? // assume that this is implemented

  //  trait FingerTree[+A] {
  //  def addL[B >:A] (b: B) :FingerTree[B] = ??? // assume that this is implemented as in the paper
  //  def addR[B >:A] (b: B) :FingerTree[B] = ??? // assume that this is implemented as in the paper
  // }

  // Implement this:

  def concatenate[A, B >: A] (left: FingerTree[A]) (right: FingerTree[B]) :FingerTree[B] = ???

} // Q7


object Q8 {

  def nullOption[T]: Lens[T, Option[T]] =
    Lens[T, Option[T]] (c => if (c == null) None else Some(c)) ((o: Option[T]) => _ => o match {
      case Some(value) => value
      case None => null.asInstanceOf[T]
    })

  // Answer the questions below:

  // A. yes. We put first, then get. since we always put first, the underlying data structure has been
  // initialised by our setter and its either not null or null. If notnull then we have set it with a value, which we
  // get

  // B. yes

  // C. yes

} // Q8

