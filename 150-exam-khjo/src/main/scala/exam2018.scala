// Name: _____________
// ITU email: ________
package adpro.exam2018

import fpinscala.monoids.Monoid
import fpinscala.monads.Monad
import fpinscala.monads.Functor
import fpinscala.laziness.{Stream,Empty,Cons}
import fpinscala.laziness.Stream._
import fpinscala.parallelism.Par._
import scala.language.higherKinds
import adpro.data._
import adpro.data.FingerTree._
import monocle.Lens

object Q1 {

  def groupByKey[K,V] (l :List[(K,V)]) :List[(K,List[V])] =
    l.foldLeft (Nil:List[(K, List[V])]) (
      (acc:List[(K, List[V])], t: (K, V)) =>
        if (acc.contains(t._1)) acc.map (e => if (t._1 == e._1) (e._1, t._2 :: e._2) else e)
        else (t._1, List(t._2))::acc
    )


  // fold over the list and for each item, map over the accumulator and add it in the correct place.
  // maybe there is an error with initialising each "sublist"

}


object Q2 {

  def f[A,B] (results: List[Either[A,B]]) :Either[List[A],List[B]] = results.foldLeft (Right(Nil: List[B]):Either[List[A], List[B]]) (
    (acc, el) => acc match {
        //if acc is a failure
      case Left(a)  => el match {
        case Left(e) => Left(e :: a)
        case Right(_) => Left(a) //we dont care if el is a success
      }
        //if acc is a success
      case Right(a) => el match {
        case Left(e) => Left(e::Nil) //if el is a failure replace acc with the failure
        case Right(e) => Right(e :: a)
      }
    }
  )
}


object Q3 {

  type T[B] = Either[String,B]
  implicit val eitherStringIsMonad :Monad[T] = new Monad[T] {
    def unit[B] (b: B):Either[String,B] = Right(b)
    override def flatMap[A, B] (a: Either[String,A]) (f: A => Either[String,B]): Either[String,B] = a.flatMap(f)
  }



  implicit def eitherIsMonad[A]: Monad[T] = {
    type T[B] = Either[A,B]
    new Monad[T] {
      def unit[B] (b: B): Either[A,B] = Right(b)
      override def flatMap[B,C] (i: Either[A,B]) (f: B => Either[A,C]): Either[A,C] = i.flatMap(f)
    }
  }

} // Q3


object Q4 {

   // Write the answers in English below.
   
   // A. The fibonacci sequence.
   // 1, 1, 2, 3, 5
   
   // B. All positive integers representable by Int. In particular, its all possible
  // consecutive pairs of fibonacci numbers
   
}


object Q5 { 

  def parForall[A] (as: List[A]) (p: A => Boolean): Par[Boolean] = map(parMap(as) (p)) ( l => l.forall(p => p))

}


object Q6 {

  def apply[F[_],A,B](fab: F[A => B])(fa: F[A]): F[B] = ???
  def unit[F[_],A](a: => A): F[A] = ???

  val f: (Int,Int) => Int = _ + _
  def a :List[Int] = ???

  //val x = apply (apply (unit (f.curried)) (a)) (a)
  // Answer below in a comment:

  // f.curried is Int => Int => Int
  // unit(f.curried) is F[Int => Int => Int]
  // apply( unit(f.curried) ) (a) is F[Int => Int]
  // apply (apply (unit (f.curried)) (a)) (a) is F[Int]

} // Q6


object Q7 {

  def map2[A,B,C] (a :List[A], b: List[B]) (f: (A,B) => C): List[C] = ???


  def map3[A,B,C,D] (a :List[A], b: List[B], c: List[C]) (f: (A,B,C) => D) :List[D] = {
    val cur = (a: A, b: B) => (c: C) => f(a, b, c)
    map2(map2(a,b) (cur), c) ((ab, c) => ab(c))
  }



  //def map3monad[A, B, C, D] :Monad[D] = ???

} // Q7


object Q8 {

  def filter[A] (t: FingerTree[A]) (p: A => Boolean): FingerTree[A] = Digit.toTree(t.toList.filter(p))

}


object Q9 {

  def eitherOption[A, B](default: => A): Lens[Either[A, B], Option[B]] = Lens[Either[A, B], Option[B]](
    {
      case Left(_) => None
      case Right(value) => Some(value)
    })(
    (s: Option[B]) => (ud: Either[A, B]) => s match {
      case Some(value) => Right(value)
      case None => Left(default)
    })


  // Answer the questions below:
  val l: Lens[Either[Int, Int], Option[Int]] = eitherOption(0)
  // A. yes. putget law says that if we put something into our lense, and then get it afterwards,
  // we should get that object. Indeed we do. In the some case of put, we save it as a Right value and retrieve that
  // in the get. For the None case we save it as a Left value, and when we retrieve a left value, we just get None

  val a = Right(10)
  val i = Some(15)
  l.get(l.set(i)(a)) == i

  // B. no. getput law says that if we get something and then put it in the lense we get the same object back. That
  // doesn't hold when we input none to a datastructure where the underlying Either is left, but with a non-default
  // value
  // l.set(l.get(Left(0)))(Left(0)) = Left(-1) if default is set to -1

  // can possibly be fixed with this
//  def eitherOption[A, B](default: => A): Lens[Either[A, B], Option[B]] = Lens[Either[A, B], Option[B]](
//    {
//      case Left(_) => None
//      case Right(value) => Some(value)
//    })(
//    (s: Option[B]) => (ud: Either[A, B]) => s match {
//      case Some(value) => Right(value)
//      case None => ud match {
//        case Left(value) => Left(value)
//        case Right(value) => Left(default)
//      }
//    })

  // C. yes. Again since we only store one value, any sequence of puts will be equal to the last put, since we
  // overwrite the value each time

} // Q9

