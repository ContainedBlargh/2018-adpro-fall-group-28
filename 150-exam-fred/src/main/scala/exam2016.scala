
package adpro.exam2016

import adpro.exam2016solution.Q4.Computation

object Q1 {
   def checksumImp (in: String) :Int = {
    var result = 0
    for (c <- in.toList)
      result = (result + c.toInt) % 0xffff
    return result
  }

  def checksumFun (in :String) :Int = {
    def go (chars: List[Char]) (sum: Int): Int = chars match {
      case Nil => sum
      case h::t => go(t) ((h.toInt + sum) % 0xffff)
    }
    go(in.toList)(0)
  } // Task 1.

  // Write your answer for Task 2 here.
}


object Q2 {
  import fpinscala.monads.Functor
  import scala.language.higherKinds

  def onList[A] (f: A => A) :List[A] => List[A] = _.map(f) // Task 3.

  def onCollection[C[_],A] (f: A => A) 
    (implicit functorC: Functor[C]) :C[A] => C[A] = c => functorC.map(c)(f) // Task 4.

}

object Q3 {

  import fpinscala.monoids.Monoid
  import scala.language.higherKinds

  def foldBack[A] (l :List[A]) (implicit M :Monoid[A]) :A = (l ++ l.reverse).foldRight (M.zero) (M.op) // Task 5.

}

object Q4 {

  type Computation[A] = A => Either[A,String]

  def run[A] (init: A) (progs: List[Computation[A]]) : (A,List[String]) = {
    def runComp[A] (state: (A,List[String]), comp: Computation[A]) :(A,List[String]) =
      comp (state._1) match {
        case Left(a) => (a, state._2)
        case Right(err) => (state._1, err::state._2)
      }
    val (a, errs) = progs.foldLeft(init, List[String]())(runComp)
    (a, errs.reverse)
  }// Task 6.
}


object Q5 {

  sealed trait Tree[A]
  case class Branch[A] (l: () => Tree[A], a: A, r:() => Tree[A]) extends Tree[A]
  case class Leaf[A] (a: A) extends Tree[A]

  def multiply (t: Tree[Int]) :Int = t match {
    case Leaf(a) => a
    case Branch(l, a, r) if a != 0  => {
      val ml = multiply(l())
      if (ml == 0) 0
      val mr = multiply(r())
      if (mr == 0) 0
      ml * a * mr
    }
    case _ => 0
  }// Task 7.

  // Task 8. (answer below in a comment)

}
   
object Q6 {
   
  sealed trait Nat[+A]
  case object Zero extends Nat[Unit]
  case class Succ[A] (pred: A) extends Nat[A]

  val zero /* : ... */ = Zero            // Task 9.
  val one  /* : ... */ = Succ (zero)     // Task 9.
  val two  /* : ... */ = Succ (one)      // Task 9.


  // def plus2  (x : ... ) : ... = ???        // Task 10.

}
