// Name: _____________
// ITU email: ________
package adpro.exam2018

import fpinscala.monoids.Monoid
import fpinscala.monads.Monad
import fpinscala.monads.Functor
import fpinscala.laziness.{Stream,Empty,Cons}
import fpinscala.laziness.Stream._
import fpinscala.parallelism.Par._
import scala.language.higherKinds
import adpro.data._
import adpro.data.FingerTree._
import monocle.Lens

object Q1 {

  def groupByKey[K,V] (l :List[(K,V)]) :List[(K,List[V])] = l.groupBy(e1 => e1._1).toList.map(e2 => (e2._1, e2._2.map(vals => vals._2)))
  def groupByKey2[K,V] (l :List[(K,V)]) :List[(K,List[V])] = {
    l.foldLeft(l.map(p => p._1).distinct.map(k => (k, List[V]())))((grouped, kv) => {
      grouped.map(g => {
        if (g._1 == kv._1) (g._1, kv._2 :: g._2)
        else g
      })
    })
  }

  // OK... so... We maintain a grouped elements List[(K, List[V])].
  // When we then fold over the list l, we map the grouped elements
  // If the element we are looking at matches a key, that value is appended to the grouped list
  // Otherwise the element is just returned


}


object Q2 { 

  def f[A,B] (results: List[Either[A,B]]) :Either[List[A],List[B]] = {
    results.foldLeft(Right(Nil: List[B]):Either[List[A], List[B]])((acc, el) => {
      acc match {
        case Left(a) => el match { // If the acc is currently left, check if the element is Left, otherwise just return the acc
          case Left(l) => Left(l::a)
          case _ => acc
        }
        case Right(b) => el match { // If the acc is currently Right, and there is another Right, add it to the acc, otherwise overwrite and restart with the Left element
          case Right(r) => Right(r::b)
          case Left(l) => Left(l::Nil)
        }
      }
    })
  }

}


object Q3 {

  type T[B] = Either[String,B]
  implicit val eitherStringIsMonad :Monad[T] = new Monad[T] {
    override def unit[B] (b: => B) :Either[String,B] = Right(b) // Man hiver ganske enkelt en B ud af røven
    override def flatMap[B,C] (ea: Either[String,B]) (f: B => Either[String,C])
    : Either[String,C] = ea flatMap f
  }



  implicit def eitherIsMonad[A] = {
    type T[B] = Either[A,B]
    new Monad[T] {
      override def unit[B] (b: => B) :Either[A,B] = Right(b)
      override def flatMap[B,C] (ea: Either[A,B]) (f: B => Either[A,C])
      : Either[A,C] = ea flatMap f
  }

} // Q3


object Q4 {

   // Write the answers in English below.
   
   // A. ...
   
   // B. ...
   
}


object Q5 { 

  def parForall[A] (as: List[A]) (p: A => Boolean): Par[Boolean] = {
    val parList : Par[List[Boolean]] = parMap (as) (p)
    map[List[Boolean], Boolean] (parList)(p => p.foldRight(true) (_&&_))
  }
}

}


object Q6 {

  def apply[F[_],A,B](fab: F[A => B])(fa: F[A]): F[B] = ???
  def unit[F[_],A](a: => A): F[A] = ???

  val f: (Int,Int) => Int = _ + _
  def a :List[Int] = ???

  // Answer below in a comment:

  // ...

} // Q6


object Q7 {

  def map2[A,B,C] (a :List[A], b: List[B]) (f: (A,B) => C): List[C] = ???


  def map3[A,B,C,D] (a :List[A], b: List[B], c: List[C]) (f: (A,B,C) => D) :List[D] = {
    map2(map2 (a, b) {case (a,b) => (a,b)}, c) {case ((a2,b2),c2) => f(a2,b2,c2)}
  }

  def map32[A,B,C,D] (a :List[A], b: List[B], c: List[C]) (f: (A,B,C) => D) :List[D] = {
    val abApplied = map2(a, b) ((a, b) => f.curried(a)(b))
    map2(c, abApplied)((c, abApplied) => abApplied(c))
  }


  def map3monad[M[_],A,B,C,D] (a:M[A], b:M[B], c:M[C]) (f: (A, B, C) => D) (implicit monad: Monad[M]): M[D] = {
    val abApplied = monad.map2(a,b)((a, b) => f.curried (a) (b))
    monad.map2(c, abApplied)((c, abApplied) => abApplied(c))
  }

} // Q7


object Q8 {

  def filter[A] (t: FingerTree[A]) (p: A => Boolean): FingerTree[A] = {
    val test : List[A] = t.toList.filter(p)
    Digit.toTree(test)
  }
}


object Q9 {

  def eitherOption[A,B] (default: => A): Lens[Either[A,B],Option[B]] = ???


  // Answer the questions below:

  // A. ...

  // B. ...

  // C. ...

} // Q9

