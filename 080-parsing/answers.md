# 080-parsing

* Frederik Madsen - fscm@itu.dk
* Kasper Berthelsen - khjo@itu.dk
* Jon Voigt - jvoi@itu.dk

We have implemented the answers for the first 5 exercises
in the file Parsers.scala, included in the submission.

We have tried our best to implement exercise 6, but have not
managed to get it running.

We have implemented a test for the json-parser in the file ParsersSpec.scala.
