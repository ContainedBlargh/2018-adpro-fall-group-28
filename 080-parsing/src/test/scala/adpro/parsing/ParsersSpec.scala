package adpro.parsing

import org.scalatest.{FlatSpec, Matchers}
import org.scalatest.prop.Checkers
import org.scalacheck._
import adpro.parsing.JSON._

class ParsersSpec extends FlatSpec with Matchers with Checkers {

  behavior of "JSONParser"

  it should "Convert json to json object containing expected values" in {
    val jsonTxt =
    """
    {
      "Company name" : "Microsoft Corporation",
      "Ticker"  : "MSFT",
      "Active"  : true,
      "Price"   : 30.66,
      "Shares outstanding" : 8.38e9,
      "Related companies" : [ "HPQ", "IBM", "YHOO", "DELL", "GOOK" ]
    }
    """
    val P = JSON.jsonParser(JSONParser)
    val jsonOrError = JSONParser.run(P)(jsonTxt)
    val json = jsonOrError.right.get
    json match {
      case JObject(map) =>
        val hasField = (s: String) => map.keySet.contains(s)
        assert(
          hasField("Company name")
            && hasField("Ticker")
            && hasField("Active")
            && hasField("Price")
            && hasField("Shares outstanding")
            && hasField("Related companies")
        )
        assert(
          map("Company name").isInstanceOf[JString]
            && map("Ticker").isInstanceOf[JString]
            && map("Active").isInstanceOf[JBool]
            && map("Price").isInstanceOf[JNumber]
            && map("Shares outstanding").isInstanceOf[JNumber]
            && map("Related companies").isInstanceOf[JArray]
        )
      case _ => assert(false)
    }
  }
}
