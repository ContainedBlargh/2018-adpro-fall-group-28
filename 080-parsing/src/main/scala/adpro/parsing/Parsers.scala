// Andrzej Wąsowski, Advanced Programming

package adpro.parsing

// I used Scala's standard library lists, and scalacheck Props in this set,
// instead of those developed by the book.

import java.util.regex._
import scala.util.matching.Regex

// we need this for higher kinded polymorphism
import language.higherKinds
// we need this for introducing internal DSL syntax
import language.implicitConversions

trait Parsers[ParseError, Parser[+ _]] {
  self =>

  def run[A](p: Parser[A])(input: String): Either[ParseError, A]

  implicit def char(c: Char): Parser[Char]

  implicit def string(s: String): Parser[String]

  implicit def operators[A](p: Parser[A]): ParserOps[A] = ParserOps[A](p)

  def or[A](s1: Parser[A], s2: => Parser[A]): Parser[A]

  def listOfN[A](n: Int, p: Parser[A]): Parser[List[A]] = if (n <= 0) succeed(List[A]()) else map2(p, listOfN(n - 1, p))((a, acc) => a :: acc)

  def map[A, B](p: Parser[A])(f: A => B): Parser[B]

  //Returns a list of consecutive elements, if any.
  def many[A](p: Parser[A]): Parser[List[A]] = or(map2(p, many(p))((a, b) => a :: b), succeed(List()))

  def succeed[A](a: A): Parser[A] = string("") map (_ => a)

  def slice[A](p: Parser[A]): Parser[String]

  def product[A, B](p: Parser[A], p2: => Parser[B]): Parser[(A, B)]

  def flatMap[A, B](p: Parser[A])(f: A => Parser[B]): Parser[B]

  implicit def regex(r: Regex): Parser[String]

  implicit def asStringParser[A](a: A)(implicit f: A => Parser[String]): ParserOps[String] = ParserOps(f(a))

  case class ParserOps[A](p: Parser[A]) {

    def |[B >: A](p2: Parser[B]): Parser[B] = self.or(p, p2)

    def or[B >: A](p2: Parser[B]): Parser[B] = self.or(p, p2)

    def **[B](p2: Parser[B]): Parser[(A, B)] = self.product(p, p2)

    def product[B](p2: Parser[B]): Parser[(A, B)] = self.product(p, p2)

    def map[B](f: A => B): Parser[B] = self.map(p)(f)

    def flatMap[B](f: A => Parser[B]): Parser[B] = self.flatMap(p)(f)

    def many: Parser[List[A]] = self.many[A](p)

    def slice: Parser[String] = self.slice(p)

    //for json parser
    def run(input: String): Either[ParseError, A] = self.run(p)(input)

    def *|[B](y: Parser[B]): Parser[A] = self.product(p, y) map (_._1)

    def |*[B](y: Parser[B]): Parser[B] = self.product(p, y) map (_._2)

    def ?(): Parser[Option[A]] = self.or(self.map(p)((o: A) => Some(o)), succeed(None))

    def *(): Parser[List[A]] = self.many(p)
  }

  object Laws {

    // Storing the laws in the trait -- the will be instantiated when we have
    // concrete implementation.  Still without a concrete implementation they
    // can be type checked, when we compile.  This tells us that the
    // construction of the laws is type-correct (the first step for them
    // passing).

    import org.scalacheck._
    import org.scalacheck.Prop._

    val runChar = Prop.forAll { c: Char => run(char(c))(c.toString) == Right(c) }
    val runString = Prop.forAll { s: String => run(string(s))(s) == Right(s) }

    val listOfN1 = Prop.protect(run(listOfN(3, "ab" | "cad"))("ababcad") == Right("ababcad"))
    val listOfN2 = Prop.protect(run(listOfN(3, "ab" | "cad"))("cadabab") == Right("cadabab"))
    val listOfN3 = Prop.protect(run(listOfN(3, "ab" | "cad"))("ababab") == Right("ababab"))
    val listOfN4 = Prop.protect(run(listOfN(3, "ab" | "cad"))("ababcad") == Right("ababcad"))
    val listOfN5 = Prop.protect(run(listOfN(3, "ab" | "cad"))("cadabab") == Right("cadabab"))
    val listOfN6 = Prop.protect(run(listOfN(3, "ab" | "cad"))("ababab") == Right("ababab"))

    def succeed[A](a: A) = Prop.forAll { s: String => run(self.succeed(a))(s) == Right(a) }

    // Not planning to run this (would need equality on parsers), but can write
    // for typechecking:

    def mapStructurePreserving[A](p: Parser[A]): Boolean =
      map(p)(a => a) == p
  }


  // Exercise 1

  def manyA[A](p: Parser[A]): Parser[Int]

  // Exercise 2

  def map2[A, B, C](p: Parser[A], p2: Parser[B])(f: (A, B) => C): Parser[C] = p ** p2 map (ab => f(ab._1, ab._2))

  def many1[A](p: Parser[A]): Parser[List[A]] = map2(p, many(p))((a, b) => b)

  // Exercise 3
  def digitTimesA[A](p: Parser[String]): Parser[Int] =
    flatMap(p)(
      ps => flatMap("\\d".r)(
        s => map(listOfN(s.toInt, char('a')))(_ => s.toInt)
      )
    )

  // Exercise 4

  def product_[A, B](p: Parser[A], p2: Parser[B]): Parser[(A, B)] = flatMap(p)(a => map(p2)(b => (a, b)))

  def map2_[A, B, C](p: Parser[A], p2: Parser[B])(f: (A, B) => C): Parser[C] = map(product_(p, p2))(ab => f(ab._1, ab._2))

  // Exercise 5

  def map_[A, B](p: Parser[A])(f: A => B): Parser[B] = flatMap(p)(a => succeed(f(a)))

  // This uses succeed which does use map. However the book describes succeed as a primitive, so thats why we allow ourselves to use it.

}

trait JSON

object JSON {

  case object JNull extends JSON

  case class JNumber(get: Double) extends JSON

  case class JString(get: String) extends JSON

  case class JBool(get: Boolean) extends JSON

  case class JArray(get: IndexedSeq[JSON]) extends JSON

  case class JObject(get: Map[String, JSON]) extends JSON

  type Parser[+A] = String => Either[String, A]

  object JSONParser extends Parsers[String, Parser] {

    def consume(input: String, amount: Int): String = input.substring(0, amount)

    def consume(input: String, toConsume: String): String = input.substring(0, toConsume.length - 1)

    implicit def char(c: Char): Parser[Char] = (input: String) => if (input == c) Right(c) else Left("Char not matched")

    // def flatMap[A, B](p: Parser[A])(f: A => Parser[B]): Parser[B] = (input: String) => run(p)(input) match {
    //   case Right(a) => {
    //     run(f(a))(input) match {
    //       case Right(s) => consume(input, )
    //       case e => e
    //     }
    //   }
    //   case Left(e) => Left(e)
    // }

    def flatMap[A, B](p: Parser[A])(f: A => Parser[B]): Parser[B] = (input: String) => run(p)(input) match {
      case Right(a) => run(f(a))(input)
      case Left(e) => Left(e)
    }

    def manyA[A](p: Parser[A]): Parser[Int] = ???

    def map[A, B](p: Parser[A])(f: A => B): Parser[B] = flatMap(p)(a => succeed(f(a)))

    def or[A](s1: Parser[A], s2: => Parser[A]): Parser[A] = (input: String) => s1(input) match {
      case Left(_) => s2(input)
      case e => e
    }

    def product[A, B](p1: Parser[A], p2: => Parser[B]): Parser[(A, B)] = flatMap(p1)(a => map(p2)(b => (a, b)))

    implicit def regex(r: Regex): Parser[String] = (input: String) => r.findPrefixOf(input) match {
      case Some(n) => Right(consume(input, n))
      case None => Left("regex match failed")
    }

    def run[A](p: Parser[A])(input: String): Either[String, A] = p(input) match {
      case Right(a) => Right(a)
      case Left(e) => Left(e)
    }

    def slice[A](p: Parser[A]): Parser[String] = ???

    implicit def string(s: String): Parser[String] = (input: String) => if (input.startsWith(s)) Right(s.substring(0, s.length - 1)) else Left("String not matched")
  }

  def jsonParser[ParseErr, Parser[+ _]](P: Parsers[ParseErr, Parser]): Parser[JSON] = {
    import P._

    val SPACES = char(' ').many.slice
    val QUOTED: Parser[String] = map("\"[^\"\"]*\"".r) {
      _ dropRight 1 substring 1
    }
    val DOUBLE: Parser[Double] = "\"(( \\+| -)?[0 -9]+( \\.[0.[0 -9]+(e[0 9]+(e[0 -9]+)?)?9]+)?)? 9]+)?)?\"".r.map {
      _.toDouble
    }

    val ws: Parser[Unit] = "[\t\n ]+".r map { _ => () }

    val jnull: Parser[JSON] = "null" |* succeed(JNull)

    val jbool: Parser[JBool] =
      (ws.? |* "true" |* succeed(JBool(true))) |
        (ws.? |* "false" |* succeed(JBool(false)))

    val jstring: Parser[JString] = QUOTED map {
      JString
    }

    val jnumber: Parser[JNumber] = DOUBLE map {
      JNumber
    }

    lazy val jarray: Parser[JArray] =
      (ws.? |* "[" |* (ws.? |* json *| ",").*
        *| ws.? *| "]" *| ws.?)
        .map { l => JArray(l.toVector) }

    lazy val field: Parser[(String, JSON)] = ws.? |* QUOTED *| ws.? *| ":" *| ws.? ** json *| ","

    lazy val jobject: Parser[JObject] =
      (ws.? |* "{" |* field.* *| ws.? *| "}" *| ws.?)
        .map { l => JObject(l.toMap) }

    lazy val json: Parser[JSON] = (jstring | jobject | jarray | jnull | jnumber | jbool) *| ws.?
    json
  }
}
