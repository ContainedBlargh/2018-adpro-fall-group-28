// Advanced Programming, Exercises by A. Wąsowski, IT University of Copenhagen
//
// AUTHOR1: khjo@itu.dk
// AUTHOR2: fscm@itu.dk
// AUTHOR3: jvoi@itu.dk
//
// Write ITU email addresses of both group members that contributed to
// the solution of the exercise (in lexicographic order).
//
// You should work with the file by following the associated exercise sheet
// (available in PDF from the course website).
//
// The file is meant to be compiled inside sbt, using the 'compile' command.
// To run the compiled file use the 'run' or 'runMain' command.
// To load the file int the REPL use the 'console' command.
// Now you can interactively experiment with your code.
//
// Continue solving exercises in the order presented in the PDF file. The file
// shall always compile, run, and pass tests, after you are done with each
// exercise (if you do them in order).  Please compile and test frequently.

// The extension of App allows writing the main method statements at the top
// level (the so called default constructor). For App objects they will be
// executed as if they were placed in the main method in Java.
package fpinscala

object Exercises extends App with ExercisesInterface {

  import fpinscala.List._

  // Exercise 3

  // add @annotation.tailrec to make the compiler check that your solution is
  // tail recursive
  def fib (n: Int) : Int = {
    @annotation.tailrec
    def fibRec (a: Int) (b: Int) (n: Int): Int = {
      n match {
        case 1 => a
        case 2 => b
        case _ => fibRec(b)(a+b)(n-1)
      }
    }
    fibRec(0)(1)(n)
  }

  // Exercise 4

  // add @annotation.tailrec to make the compiler check that your solution is
  // tail recursive
  def isSorted[A] (as: Array[A], ordered: (A,A) =>  Boolean) :Boolean = {
    @annotation.tailrec
    def isSortedRec (acc: Boolean, p: Int): Boolean = {
      if (as.length == 0) {
        return true
      }
      if (p + 1 == as.length) {
        return acc
      } else {
        return isSortedRec( acc && ordered(as(p), as(p + 1)), p + 1)
      }
    }
    isSortedRec (true, 0)
  }

  // Exercise 5

  def curry[A,B,C] (f: (A,B)=>C): A => (B => C) = (a: A) => (b: B) => f(a, b)

  // Exercise 6

  def uncurry[A,B,C] (f: A => B => C): (A,B) => C = (a: A, b: B) => f (a) (b)

  // Exercise 7

  def compose[A,B,C] (f: B => C, g: A => B) : A => C = (a: A) => f (g (a))

  // Exercise 8 requires no programming

  /*
   The expression should return 3 or, rather x + y.
   That is because List(1, 2, 3, 4, 5) can be constructed from
   4 Cons() expressions.
  */

  // Exercise 9

  def tail[A] (as: List[A]) :List[A] = 
    as match {
      case Nil => throw new IllegalArgumentException
      case Cons(_, tail) => tail
    }

  /*
    If someone asks for the tail of an empty list, it is highly likely that they've made
    a logical error in their program. Therefore,  more correct solution would be to throw
    an IllegalArgumentException. BUT, this is not purely functional!
    Instead, the signature of the function should wrap the return value in an option type, 
    meaning that the function returns a list, if such was available.
    Returning the empty list is also logically sound most of the time, and would actually
    be what you would get for a single-item list.
  */

  // Exercise 10

  @annotation.tailrec
  // Uncommment the annotation after solving to make the
  // compiler check whether you made the solution tail recursive
  def drop[A] (l: List[A], n: Int) : List[A] = 
    n match {
      case n if (n <= 0) => l
      case _ => drop(tail(l), n - 1)
    }

  // Exercise 11

  @annotation.tailrec
  // Uncommment the annotation after solving to make the
  // compiler check whether you made the solution tail recursive
  def dropWhile[A](l: List[A], f: A => Boolean): List[A] = 
    l match {
      case Cons(head, tail) if (f (head)) => dropWhile(tail, f)
      case _ => l
    }

  // Exercise 12

  def init[A](l: List[A]): List[A] =
    l match {
      case Nil => throw new IllegalArgumentException
      case Cons(_, Nil) => Nil
      case Cons(x, xs) => Cons(x, init(xs))
    }

  /*
    As with any singly-linked list, we can only use the cons operation in one direction,
    meaning that we have to accumulate the initial elements of the list in a new list.
    The function is neither constant time nor constant space
    The running time is O(N). 
    The space consumption grows for each recursive call, making the total space consumption during
    the run O(N^2).
  */

  // Exercise 13

  def length[A] (as: List[A]): Int = {
    List.foldRight (as, 0) ( (_, n) => n + 1 )
  }

  // Exercise 14

/*
  The foldRight implementation was not tailrecursive, which will lead to a stackoverflow faster.
  This implementation fo fold is not stack-overflow free though, a continuation function would be required
  to achieve this (making the function depend on heap memory, which will make it slower).
*/

  @annotation.tailrec
  // Uncommment the annotation after solving to make the
  // compiler check whether you made the solution tail recursive
  def foldLeft[A,B] (as: List[A], z: B) (f: (B, A) => B): B = 
    as match {
      case Nil => z
      case Cons(x, xs) => foldLeft(xs, f (z, x)) (f)
    }

  // Exercise 15

  def product (as: List[Int]): Int = foldLeft (as, 1) ((product, x) => product * x )

  def length1 (as: List[Int]): Int = foldLeft (as, 0) ((n, _) => n + 1 )

  // Exercise 16
/*
  We get it, everything can be solved with fold.
*/
  def reverse[A] (as: List[A]): List[A] = foldLeft (as, List[A]()) ((acc: List[A], x: A) => Cons(x, acc))

  // Exercise 17

  def foldRight1[A,B] (as: List[A], z: B) (f: (A, B) => B): B = foldLeft (reverse(as), z) ( (b: B, a: A) => f(a, b) )

  // Exercise 18

/*
  This looks right, but we're not sure.
*/
  def foldLeft1[A,B] (as: List[A], z: B) (f: (B,A) => B): B = {
    val rightRun = foldRight1 (as, (b: B) => b) ( (a: A, fun: B => B) => (b: B) => fun (f (b, a) ) )
    rightRun(z)
  }

  // Exercise 19

  def append[A](a1: List[A], a2: List[A]): List[A] = a1 match {
    case Nil => a2
    case Cons(h,t) => Cons(h, append(t, a2))
  }

  //This operation ought to be called flatten
  def concat[A] (as: List[List[A]]): List[A] = foldLeft (as, List[A]()) ((acc, l) => append(acc, l))

  // Exercise 20

  def filter[A] (as: List[A]) (f: A => Boolean): List[A] = 
    foldLeft (as, List[A]()) ((acc, a) => if (f(a)) { append(acc, List(a)) } else { acc })

  // Exercise 21

  def flatMap[A,B](as: List[A])(f: A => List[B]): List[B] =  concat(foldLeft (as, List[List[B]]()) ( (acc: List[List[B]], a: A) => append(acc, List(f (a)))))

  // Exercise 22

  def filter1[A] (l: List[A]) (p: A => Boolean) :List[A] = flatMap (l) ( (a: A) => if (p(a)) { List(a) } else { Nil } )

  // Exercise 23

  def add (l: List[Int]) (r: List[Int]): List[Int] = zipWith ((a: Int, b: Int) => a + b) (l, r)

  // Exercise 24

  def zipWith[A,B,C] (f: (A,B)=>C) (l: List[A], r: List[B]): List[C] = {
    def zipWithRec (acc: List[C]) (lrem: List[A]) (rrem: List[B]): List[C] = {
      (lrem, rrem) match {
        case (Nil, Nil) => acc
        case (Nil, _) => acc
        case (_, Nil) => acc
        case (Cons(x, xs), Cons(y, ys)) => zipWithRec (Cons((f (x, y)), acc)) (xs) (ys)
      }
    }
    reverse((zipWithRec (Nil) (l) (r))) //Much cheaper than using append, which would have been N * N, instead we get 2 * N
  }

  // Exercise 25

  def head[A] (l: List[A]) = 
    l match {
      case Nil => Nil
      case Cons(x, _) => x
    }

  def hasSubsequence[A] (sup: List[A], sub: List[A]): Boolean = {
    if (sub == Nil) {
      return true
    }

    def checkRem (supRem: List[A]) (sub: List[A]): Boolean = {
      foldLeft ((zipWith ((x:A, y:A) => x == y) (supRem, sub)), true) ((acc, v) => acc && v)
      //Ak og ve, haahahahahaha
    }

    def checkSeq (supRem: List[A]): Boolean = {
      supRem match {
        case Nil => false
        case Cons(x, xs) if (x == (head (sub))) => { 
          if(checkRem (supRem) (sub)) {
            return true
          } else {
            checkSeq (xs)
          }
        }
        case Cons(_, xs) => checkSeq (xs)
      }
    }
    checkSeq (sup)
  }


  // Exercise 26

  def pascal (n: Int): List[Int] = {
    n match {
      case 1 => List(1)
      case 2 => List(1, 1)
      case a => {
        def f (l: List[Int]): List[Int] = {
          l match {
            case Nil => l
            case Cons(_, Nil) => l
            case Cons(x, Cons(y, xs)) => Cons(x + y, f(Cons(y, xs)))
          }
        }
        val prev = pascal(a - 1)
        append (Cons(1, f(prev)), List(1))
      }
    }
  }
}