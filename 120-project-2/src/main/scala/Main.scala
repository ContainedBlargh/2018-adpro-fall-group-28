// Advanced Programming. Andrzej Wasowski. IT University
// To execute this example, run "sbt run" or "sbt test" in the root dir of the project
// Spark needs not to be installed (sbt takes care of it)

import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.classification.MultilayerPerceptronClassifier
import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator
import org.apache.spark.ml.feature.{RegexTokenizer, StopWordsRemover, Tokenizer, VectorAssembler}
import org.apache.spark.ml.linalg.Vectors
import org.apache.spark.ml.tuning.{CrossValidator, ParamGridBuilder}
import org.apache.spark.mllib.evaluation.MulticlassMetrics
import org.apache.spark.sql.{Dataset, Encoder, Row, SparkSession}
import org.apache.spark.sql.types._

object Main {
  org.apache.log4j.Logger getLogger "org" setLevel (org.apache.log4j.Level.WARN)
  org.apache.log4j.Logger getLogger "akka" setLevel (org.apache.log4j.Level.WARN)
  val spark = SparkSession.builder
    .appName("Sentiment")
    .master("local[9]")
    .getOrCreate


  case class Embedding(word: String, vector: Array[Double])

  case class ParsedReview(id: Long, text: String, score: Double)

  case class ScoredTokens(reviewId: Long, tokens: Array[String], score: Double)

  case class ReviewVector(reviewId: Long, averageVector: Array[Double])

  /**
    * Converts a score to a rating
    *
    * @param score Overall score from the review dataset.
    * @return 0 for negative, 1 for neutral and 2 for positive.
    */
  def scoreToRating(score: Double): Int = {
    score match {
      case neg if neg <= 2.0 => 0 //Negative
      case neu if neu > 2.0 && neu < 4.0 => 1 //Neutral
      case pos if pos >= 4 => 2 //Positive
    }
  }

  case class SentimentData(reviewId: Long, averageVector: org.apache.spark.ml.linalg.Vector, rating: Int)

  import spark.implicits._

  val reviewSchema = StructType(Array(
    StructField("reviewText", StringType, nullable = false),
    StructField("overall", DoubleType, nullable = false),
    StructField("summary", StringType, nullable = false)))

  // Read file and merge the text abd summary into a single text column

  def loadReviews(path: String): Dataset[ParsedReview] =
    spark
      .read
      .schema(reviewSchema)
      .json(path)
      .rdd
      .zipWithUniqueId
      .map[(Integer, String, Double)] { case (row, id) => (id.toInt, s"${row getString 2} ${row getString 0}", row getDouble 1) }
      .toDS
      .withColumnRenamed("_1", "id")
      .withColumnRenamed("_2", "text")
      .withColumnRenamed("_3", "score")
      .as[ParsedReview]

  def tokenizeReviews(reviews: Dataset[ParsedReview]): Dataset[ScoredTokens] = {
    val tokenizer = new Tokenizer().setInputCol("text").setOutputCol("tokens")

    val stopRemover = new StopWordsRemover().setInputCol("tokens").setOutputCol("no_stopwords")

    val tokenized = tokenizer.transform(reviews)
    stopRemover.transform(tokenized).map((r: Row) =>
      ScoredTokens(
        r.getAs[Int]("id"),
        r.getAs[List[String]]("no_stopwords").toArray,
        r.getAs[Double]("score")
      ))
  }

  // Load the GLoVe embeddings file

  def loadGlove(path: String): Dataset[Embedding] =
    spark
      .read
      .text(path)
      .map {
        _ getString 0 split " "
      }
      .map(r => (r.head, r.tail.toList.map(_.toDouble))) // yuck!
      .withColumnRenamed("_1", "word")
      .withColumnRenamed("_2", "vector")
      .as[Embedding]

  /**
    * Type safe way of joining two tables.
    *
    * @param ds1 first dataset
    * @param ds2 seconds dataset
    * @param f   key extractor for ds1
    * @param g   key extractor for ds2
    * @tparam T ds1 generic type
    * @tparam U ds2 generic type
    * @tparam K key type
    * @return a dataset combined from the inputs
    */
  def safeEquiJoin[T, U, K](ds1: Dataset[T], ds2: Dataset[U])
                                   (f: T => K, g: U => K)
                                   (implicit e1: Encoder[(K, T)], e2: Encoder[(K, U)], e3: Encoder[(T, U)]) = {
    val ds1_ = ds1.map(x => (f(x), x))
    val ds2_ = ds2.map(x => (g(x), x))
    ds1_.joinWith(ds2_, ds1_("_1") === ds2_("_1")).map(x => (x._1._2, x._2._2))
  }

  /**
    * Extract sentiment data from the reviews, scoredTokens and the GLoVe file.
    *
    * @param reviews      a dataset containing the parsed reviews.
    * @param scoredTokens a dataset containing the scored tokens.
    * @param glove        a dataset containing the embeddings.
    * @return a dataset of SentimentData
    */
  def extractSentimentData(reviews: Dataset[ParsedReview])
                                  (scoredTokens: Dataset[ScoredTokens])
                                  (glove: Dataset[Embedding]): Dataset[SentimentData] = {
    val reviewVectors = safeEquiJoin(scoredTokens.flatMap(s => s.tokens.map(t => (s.reviewId, t))), glove)(p => p._2, e => e.word)
      .map(p => (p._1._1, p._2.vector))
      .groupByKey(_._1)
      .reduceGroups((l, r) => (l._1, l._2.indices.map(i => (l._2(i) + r._2(i)) / 2).toArray))
      .map(sd => ReviewVector(sd._2._1, sd._2._2))

    safeEquiJoin(reviewVectors, reviews.map(r => (r.id, r.score)))(rv => rv.reviewId, r => r._1)
      .map(p => SentimentData(p._1.reviewId, Vectors.dense(p._1.averageVector), scoreToRating(p._2._2)))
  }

  /**
    * Class for wrapping the program arguments.
    *
    * @param reviewsPath  path to the reviews file.
    * @param glovePath    path to the GLoVe file.
    * @param iterations   max iterations for the perceptron
    * @param hiddenLayers hidden layer configuration for the perceptron
    * @param split        data percentages for the
    */
  private case class Params(
                     reviewsPath: String,
                     glovePath: String,
                     iterations: Int,
                     hiddenLayers: Array[Int],
                     split: Array[Double]
                   )

  //DEFAULT VALUES
  private val ITERATIONS = 100
  private val HIDDEN_LAYER = Array(100, 25)
  private val DATA_SPLIT = Array(0.1, 0.9)

  /**
    * Parses the program arguments
    *
    * @param args the program arguments
    * @return the parameters for the program
    */
  private def parseArgs(args: Array[String]): Params = {
    val reviewsPath = args.head
    val glovePath = args.tail.head

    def parseArgsRec(args: List[String])(iterations: Int)(layerConfig: Array[Int])(split: Array[Double]): Params = {
      args match {
        case Nil => Params(reviewsPath, glovePath, iterations, layerConfig, split)
        case x :: xs => x match {
          case "-i" => parseArgsRec(xs.tail)(xs.head.toInt)(layerConfig)(split)
          case "-h" => parseArgsRec(xs.tail)(iterations)(xs.head.split(",").map(_.toInt))(split)
          case "-s" =>
            val customSplit = xs.head.split(",").map(_.toDouble)
            val splitToSet = if (customSplit.sum.equals(1.0)) customSplit else DATA_SPLIT
            parseArgsRec(xs.tail)(iterations)(layerConfig)(splitToSet)
        }
      }
    }

    parseArgsRec(args.drop(2).toList)(ITERATIONS)(HIDDEN_LAYER)(DATA_SPLIT)
  }

  def main(args: Array[String]): Unit = {

    if (args.isEmpty) {
      spark.stop()
      val help = "Usage of this program:\n" +
        "sbt run <path to review file (.json or .json.gz)> <path to GLoVe file> <optional params (if any)>\n" +
        "optional parameters:\n" +
        "flag | type    | description                              | example\n" +
        "-i   | integer | training iterations                      | -i 100\n" +
        "-h   | string  | hidden layer configuration               | -l \"100, 25\"\n" +
        "-s   | string  | data split percentages (test, training)  | -s \"0.1, 0.9\"\n"
      print(help)
      return
    }

    val params = parseArgs(args)

    // "./data/Musical_Instruments_5.json"
    val reviews = loadReviews(params.reviewsPath)

    val scoredTokens = tokenizeReviews(reviews)

    //"./data/glove.6B.300d.txt"
    val glove = loadGlove(params.glovePath)

    val sentimentData = extractSentimentData(reviews)(scoredTokens)(glove)



    val layers = (List(300) ++ params.hiddenLayers ++ List(3)).toArray
    val trainer = new MultilayerPerceptronClassifier()
      .setLayers(layers)
      .setBlockSize(128)
      .setSeed(1234L)
      .setMaxIter(params.iterations)

    val inputDataSet = sentimentData
      .withColumnRenamed("averageVector", "features")
      .withColumnRenamed("rating", "label")
      .randomSplit(params.split)



    val testData = inputDataSet.head
    val trainData = inputDataSet.last

    //val model = trainer.fit(trainData)
//


    val evaluator = new MulticlassClassificationEvaluator()

    val paramMap = new ParamGridBuilder().build()

    val crossValidator = new CrossValidator()
      .setEstimator(trainer)
      .setEvaluator(evaluator)
      .setEstimatorParamMaps(paramMap)
      .setNumFolds(10)

    val model = crossValidator.fit(trainData)
    val avgMetrics = model.avgMetrics
    println(s"avgMetrics: ${avgMetrics.mkString("\n")}")

    val result = model.transform(testData)

    val predictionAndLabels = result.select("prediction", "label")
    //The metrics were found at https://spark.apache.org/docs/2.4.0/mllib-evaluation-metrics.html

    val metrics = new MulticlassMetrics(predictionAndLabels.rdd.map(r =>
      (r.getAs[Double]("prediction"), r.getAs[Int]("label").toDouble))
    )

    // Confusion matrix
    println("Confusion matrix:")
    println(metrics.confusionMatrix)

    // Overall Statistics
    val accuracy = metrics.accuracy
    println("Summary Statistics")
    println(s"Accuracy = $accuracy")

    // Precision by label
    val labels = metrics.labels
    labels.foreach { l =>
      println(s"Precision($l) = " + metrics.precision(l))
    }

    // Recall by label
    labels.foreach { l =>
      println(s"Recall($l) = " + metrics.recall(l))
    }

    // False positive rate by label
    labels.foreach { l =>
      println(s"FPR($l) = " + metrics.falsePositiveRate(l))
    }

    // F-measure by label
    labels.foreach { l =>
      println(s"F1-Score($l) = " + metrics.fMeasure(l))
    }

    // Weighted stats
    println(s"Weighted precision: ${metrics.weightedPrecision}")
    println(s"Weighted recall: ${metrics.weightedRecall}")
    println(s"Weighted F1 score: ${metrics.weightedFMeasure}")
    println(s"Weighted false positive rate: ${metrics.weightedFalsePositiveRate}")

    spark.stop
  }
}
