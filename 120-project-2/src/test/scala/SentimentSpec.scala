import org.scalatest.{FreeSpec, Matchers, BeforeAndAfterAll}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.Dataset

class SentimentSpec extends FreeSpec with Matchers with BeforeAndAfterAll {

  org.apache.log4j.Logger getLogger "org"  setLevel (org.apache.log4j.Level.WARN)
  org.apache.log4j.Logger getLogger "akka" setLevel (org.apache.log4j.Level.WARN)

  val spark =  SparkSession.builder
    .appName ("Sentiment")
    .master  ("local[12]")
    .getOrCreate

  override def afterAll = spark.stop

  import spark.implicits._
  import scala.collection.JavaConversions._


  val reviews = Main.loadReviews ("./data/Patio_Lawn_and_Garden_5.json")
  val glove  = Main.loadGlove ("./data/glove.6B.300d.txt")

  "test suite" - {
    "test tokenization of reviews" - {
      val tokens = Main.tokenizeReviews(reviews)

    }
    "test safeEquiJoin of two tables" - {
      val leftSeq = Seq(1, 2, 3, 4, 5)
      val rightSeq = Seq('A', 'B', 'C', 'D', 'E')
      val combined = leftSeq.zip(rightSeq).map(p => s"${p._1}${p._2}")
      val left = spark.createDataset(leftSeq)
      val right = spark.createDataset(rightSeq)

      Main.safeEquiJoin(left, right)(identity, identity)
        .map(p => s"${p._1}${p._2}")
        .collectAsList()
        .toList
        .forall(combined.contains(_))
    }
  }

}
