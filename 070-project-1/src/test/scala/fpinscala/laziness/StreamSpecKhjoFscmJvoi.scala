// Advanced Programming
// Andrzej Wasowski, IT University of Copenhagen

package fpinscala.laziness

import scala.language.higherKinds

import org.scalatest.FlatSpec
import org.scalatest.prop.Checkers
import org.scalacheck._
import org.scalacheck.Prop._
import Arbitrary.arbitrary

// If you comment out all the import lines below, then you test the Scala
// Standard Library implementation of Streams. Interestingly, the standard
// library streams are stricter than those from the book, so some laziness tests
// fail on them :)

import stream00._    // uncomment to test the book solution
//import stream01._ // uncomment to test the broken headOption implementation
//import stream02._ // uncomment to test another version that breaks headOption

class StreamSpecKhjoFscmJvoi extends FlatSpec with Checkers {

  import Stream._

  behavior of "headOption"

  // a scenario test:

  it should "return None on an empty Stream (01)" in {
    assert(empty.headOption.isEmpty)
  }

  // An example generator of random finite non-empty streams
  def list2stream[A](la: List[A]): Stream[A] = la.foldRight(empty[A])(cons[A](_, _))

  // In ScalaTest we use the check method to switch to ScalaCheck's internal DSL
  def genNonEmptyStream[A](implicit arbA: Arbitrary[A]): Gen[Stream[A]] =
    for {la <- arbitrary[List[A]] suchThat (_.nonEmpty)}
      yield list2stream(la)


  // a property test:

  it should "return the head of the stream packaged in Some (02)" in check {
    // the implict makes the generator available in the context
    implicit def arbIntStream: Arbitrary[Stream[Int]] = Arbitrary[Stream[Int]](genNonEmptyStream[Int])

    ("singleton" |:
      forAll { n: Int => cons(n, empty).headOption.contains(n) }) &&
      ("random" |:
        forAll { s: Stream[Int] => s.headOption.isDefined })

  }

  it should "not force the tail of the stream" in check {
    implicit def arbIntStream: Arbitrary[Stream[Int]] = Arbitrary[Stream[Int]] (genNonEmptyStream[Int])
    // a hacky way of testing it. Make it fail if evaluated
    // TODO: fix this so it doesn't use append
    Prop.forAll {(i: Int, n: Stream[Int]) => cons(i,n.append(constant(1/0))).headOption; true}
  }


  behavior of "take"

  it should "not force any heads nor any tails of the Stream" in check {
    implicit def arbFailingIntStream: Arbitrary[Stream[Int]] = Arbitrary[Stream[Int]](genNonEmptyStream[Int])

    "random" |: forAll { s: Stream[Int] => s.map(_ / 0).take(1).isInstanceOf[Stream[Int]] }
  }

  it should "never force (n+1)st item if take(n) forced" in check {
    implicit def arbMostlyWorkingIntStream: Arbitrary[Stream[Int]] = Arbitrary[Stream[Int]](genNonEmptyStream[Int])

    "random" |: forAll { s: Stream[Int] => {
      val size = s.toList.size
      val other = Stream[Int](1).map(_ / 0)
      s.append(other)
        .take(size)
        .toList
        .nonEmpty
      }
    }
  }

  it should "s.take(n).take(n) == s.take(n) for any Stream s and any n" in check {
    implicit def arbWorkingIntStreams: Arbitrary[Stream[Int]] = Arbitrary[Stream[Int]] (genNonEmptyStream[Int])
    Prop.forAll { (n: Int, s: Stream[Int]) =>  (n >= 0) ==> (s.take(n).take(n).toList == s.take(n).toList) }
  }

  
  behavior of "append"

  it should "still be empty when already empty and appended with empty stream" in {
    assert(empty.append(empty).toList.isEmpty)
  }

  it should "return the list when an empty element is appended to the list" in check {
    implicit def arbWorkingIntStreams: Arbitrary[Stream[Int]] = Arbitrary[Stream[Int]] (genNonEmptyStream[Int])
    Prop.forAll {n: Stream[Int] => n.append(empty).toList == n.toList}
  }


  it should "return list 1 + list 2 when 2 lists are appended" in check {
    implicit def arbWorkingIntStreams: Arbitrary[Stream[Int]] = Arbitrary[Stream[Int]] (genNonEmptyStream[Int])
    Prop.forAll {(n: Stream[Int], p: Stream[Int]) => n.append(p).toList == n.toList ++ p.toList }
  }

  it should "return the list when the list is appended on an empty stream" in check {
    implicit def arbWorkingIntStreams: Arbitrary[Stream[Int]] = Arbitrary[Stream[Int]] (genNonEmptyStream[Int])
    Prop.forAll {n: Stream[Int] => Stream.empty.append(n).toList == n.toList}
  }

  //TODO test if:
  //identity. c.Append(Nil) == c
  //single. constant(hej).Append(constant(meddig)) == cons(hej, cons(meddig, Nil))
  //multiple. 
  //append on empty stream. (what should happen?)

  behavior of "drop"

  it should "s.drop(n).drop(m) == s.drop(n+m) for any n, m (additivity)" in check {
    // the implict makes the generator available in the context
    implicit def arbIntStream: Arbitrary[Stream[Int]] = Arbitrary[Stream[Int]] (genNonEmptyStream[Int])
    implicit def arbInt: Arbitrary[Int] = Arbitrary[Int] (Gen.choose(0,Int.MaxValue / 2))
      Prop.forAll { (n: Int, m: Int, s :Stream[Int]) => (s.drop(n).drop(m).toList == s.drop(n+m).toList)
        }
      }

  it should "s.drop(n) does not force any of the dropped elements heads" in check {
    // the implict makes the generator available in the context
    implicit def arbIntStream: Arbitrary[Stream[Int]] = Arbitrary[Stream[Int]] (genNonEmptyStream[Int])
    ("singleton" |: Prop.forAll { n :Int =>
      val ns =
        cons(constant(n/0), cons(n, empty)).drop(0)
      val gg = ns.drop(1).toList.head
      true
    }) &&
    ("random" |: Prop.forAll { s :Stream[Int] =>
      val ns = cons(constant(1/0), s).drop(0)
      val gg = ns.drop(1).toList.head
      true
    })
  }

  behavior of "map"

  it should "return self when ran with the identity funtion" in check {
    implicit def arbIntStream: Arbitrary[Stream[Int]] = Arbitrary[Stream[Int]] (genNonEmptyStream[Int])
    Prop.forAll {n: Stream[Int] => n.map(x => x).toList == n.toList}
  }

  it should "terminate on infinite streams" in {
    lazy val ones: Stream[Int] = Stream.cons(1, ones)
    ones.map(x=>x)
    succeed
  }

}

